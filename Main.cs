﻿using GTA;
using GTA.Math;
using GTA.Native;
using System.IO;
using System.Windows.Forms;

namespace ForceEject
{
    public class ForceEject : Script
    {
        private const string INI_SETTINGS_SECTION_STRING = "SETTINGS";

        private float force;
        bool forcePedsNoVeh;
        private Keys ejectKey;

        public ForceEject()
        {
            string settingsFileName = Filename.Substring(0, Filename.LastIndexOf(".")) + ".ini";
            if (!File.Exists(settingsFileName)) UI.ShowSubtitle("INI file does not exist. Using default values (B to eject).", 5000);

            force = Settings.GetValue(INI_SETTINGS_SECTION_STRING, "FORCE", 150.0f);
            forcePedsNoVeh = Settings.GetValue(INI_SETTINGS_SECTION_STRING, "FORCE_PEDS_WHEN_NOT_IN_VEH", false);
            ejectKey = Settings.GetValue(INI_SETTINGS_SECTION_STRING, "EJECT_KEY", Keys.B);

            KeyUp += ForceEject_KeyUp;
        }

        private Vector3 GetModelMaxDimensions(int hash)
        {
            OutputArgument outArgMin = new OutputArgument(), outArgMax = new OutputArgument();
            Function.Call(Hash.GET_MODEL_DIMENSIONS, (uint)hash, outArgMin, outArgMax);
            return outArgMax.GetResult<Vector3>();
        }

        private void EjectPed(Ped ped)
        {
            Vehicle vehicleWasIn = null;

            if (ped.IsInVehicle()) vehicleWasIn = ped.CurrentVehicle;

            ped.Task.ClearAllImmediately();

            for (int i = 0; i < 2000; i++)
            {
                if (!ped.IsInVehicle())
                {
                    break;
                }

                Wait(1);
            }

            if (!ped.IsInVehicle())
            {
                Vector3 newPos;

                if (vehicleWasIn != null)
                {
                    Vector3 dimensions = GetModelMaxDimensions(vehicleWasIn.Model.Hash);
                    newPos = vehicleWasIn.GetOffsetInWorldCoords(new Vector3(dimensions.X / 4f, dimensions.Y / 2.0f, dimensions.Z));
                }
                else
                {
                    newPos = ped.Position += new Vector3(0f, 0f, 5f);
                }

                ped.Position = newPos;

                //ped.Euphoria.HighFall.Start(3500); //crashes the game on newer gta v versions

                Function.Call<bool>(Hash.SET_PED_TO_RAGDOLL, ped, 10000, 10000, 0, true, true, false);
                
                ped.ApplyForce(Vector3.WorldUp * force);
                Function.Call(Hash.PLAY_PAIN, ped, 6, 0, 0);
            }
        }

        private void ForceEject_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == ejectKey)
            {
                Ped playerPed = Game.Player.Character;

                OutputArgument outArg = new OutputArgument();
                bool foundEntity = Function.Call<bool>(Hash.GET_ENTITY_PLAYER_IS_FREE_AIMING_AT, Game.Player, outArg);

                if (foundEntity)
                {
                    Entity entity = outArg.GetResult<Entity>();

                    if (entity.Exists() && entity.IsAlive && Function.Call<bool>(Hash.IS_ENTITY_A_PED, entity))
                    {
                        Ped ped = Function.Call<Ped>(Hash.GET_PED_INDEX_FROM_ENTITY_INDEX, entity);

                        if (ped != playerPed && (ped.IsInVehicle() || forcePedsNoVeh))
                        {
                            EjectPed(ped);
                        }
                    }
                }
            }
        }
    }
}
